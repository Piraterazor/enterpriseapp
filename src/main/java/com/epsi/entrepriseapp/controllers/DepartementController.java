package com.epsi.entrepriseapp.controllers;

import com.epsi.entrepriseapp.entities.Departement;
import com.epsi.entrepriseapp.services.DepartementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Component
@Api(tags = "Department Management")
public class DepartementController {

    //CREATE SERVICE
    @Autowired
    private DepartementService departementService;

    //INITIALISATION SERVICE
    @Autowired
    public DepartementController(final DepartementService departementService){
        this.departementService = departementService;
    }

    //CREATE
    @RequestMapping(value = "/departement", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Add a department")
    void addDepartement(@RequestBody Departement departement){
        this.departementService.create(departement);
    }

    //READ
    @RequestMapping(value = "/departement/{idDepartement}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get department by id")
    Departement getDepartementById(@PathVariable final Integer idDepartement) {
        return this.departementService.read(idDepartement);
    }

    //UPDATE
    @RequestMapping(value = "/departement/{idDepartement}/", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update department")
    void updateDepartement(@PathVariable final Integer idDepartement, @PathVariable final Departement departement){
        Departement currentDepartement = departementService.read(idDepartement);
        if(departement.getNomDepartement() != null){
            currentDepartement.setNomDepartement(departement.getNomDepartement());
        }
        if(departement.getBudgetDepartement() != null){
            currentDepartement.setBudgetDepartement(departement.getBudgetDepartement());
        }
       departementService.update(currentDepartement);
    }

    //DELETE
    @RequestMapping(value = "/departement/{idDepartement}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete a department")
    void deleteDepartement(@PathVariable final Integer idDepartement) {
        departementService.delete(idDepartement);
    }

    //READ ALL
    @RequestMapping(value = "/departements", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get all departments")
    List<Departement> getAllDepartements() {
        return this.departementService.readAll();
    }
}
