package com.epsi.entrepriseapp.controllers;

import com.epsi.entrepriseapp.entities.Employe;
import com.epsi.entrepriseapp.services.EmployeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Component
@Api(tags = "Employee Management")
public class EmployeController {

    //CREATE SERVICE
    @Autowired
    private EmployeService employeService;

    //INITIALISATION SERVICE
    @Autowired
    public EmployeController(final EmployeService employeService){
        this.employeService = employeService;
    }

    //CREATE
    @RequestMapping(value = "/employe", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Add an employee")
    void addEmploye(@RequestBody Employe employe){
        this.employeService.create(employe);
    }

    //READ
    @RequestMapping(value = "/employe/{idEmploye}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get employe by Id")
    Employe getEmployeById(@PathVariable final Integer idEmploye) {
        return this.employeService.read(idEmploye);
    }

    //UPDATE
    @RequestMapping(value = "/employe/{idEmploye}/", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update an employee")
    void updateEmploye(@PathVariable final Integer idEmploye, @PathVariable final Employe employe){
        Employe currentEmploye = employeService.read(idEmploye);
        if (employe.getNomEmploye() != null){
            currentEmploye.setNomEmploye(employe.getNomEmploye());
        }
        if (employe.getPrenomEmploye() != null){
            currentEmploye.setPrenomEmploye(employe.getPrenomEmploye());
        }
        if (employe.getPrenomEmploye() != null){
            currentEmploye.setPrenomEmploye(employe.getPrenomEmploye());
        }
        if (employe.getDateEmbaucheEmploye() != null){
            currentEmploye.setDateEmbaucheEmploye(employe.getDateEmbaucheEmploye());
        }
        if (employe.getSalaireEmploye() != null){
            currentEmploye.setSalaireEmploye(employe.getSalaireEmploye());
        }
        if (employe.getPosteEmploye() != null){
            currentEmploye.setPosteEmploye(employe.getPosteEmploye());
        }
        if (employe.getEmailEmploye() != null){
            currentEmploye.setEmailEmploye(employe.getEmailEmploye());
        }
        if (employe.getTelephoneEmploye() != null){
            currentEmploye.setTelephoneEmploye(employe.getTelephoneEmploye());
        }
        if (employe.getMotDePasse() != null){
            currentEmploye.setMotDePasse(employe.getMotDePasse());
        }
        if (employe.getDepartement() != null){
            currentEmploye.setDepartement(employe.getDepartement());
        }
        if (employe.getEntreprise() != null){
            currentEmploye.setEntreprise(employe.getEntreprise());
        }
       employeService.update(currentEmploye);
    }

    //DELETE
    @RequestMapping(value = "/employe/{idEmploye}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete an employee")
    void deleteEmploye(@PathVariable final Integer idEmploye) {
        employeService.delete(idEmploye);
    }

    //READ ALL
    @RequestMapping(value = "/employes", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get all employees")
    List<Employe> getAllEmployes() {
        return this.employeService.readAll();
    }

    //GET EMPLOYEES BY NAME
    @RequestMapping(value = "/employename/{nomEmploye}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get an employee from his name")
    List<Employe> getEmployesByName(@PathVariable final String nomEmploye) {
        return this.employeService.getEmployesByName(nomEmploye);
    }

    //GET EMPLOYEES BY IDDEPARTEMENT
    @RequestMapping(value = "/employesbydepartement/{idDepartement}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get an employee by his department")
    List<Employe> getEmployesByName(@PathVariable final int idDepartement) {
        return this.employeService.getEmployesByDepartementId(idDepartement);
    }

    //PASSWORD CHECK
    @RequestMapping(value = "/passwordCheck/{email}/{motDePasse}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Check if combination of email and password is true")
    Boolean passwordCheck(@PathVariable final String email, @PathVariable final String motDePasse) {
        Employe currentEmploye = employeService.getEmployeByEmail(email);
        if (currentEmploye != null) {
            String currentPw = currentEmploye.getMotDePasse();
            if (motDePasse.equals(currentPw)) {
                return true;
            }
        }
        return false;
    }
}
