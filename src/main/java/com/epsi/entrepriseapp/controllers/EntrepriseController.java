package com.epsi.entrepriseapp.controllers;

import com.epsi.entrepriseapp.entities.Entreprise;
import com.epsi.entrepriseapp.services.EntrepriseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Component
@Api(tags = "Enterprise Management")
public class EntrepriseController {

    //CREATE SERVICE
    @Autowired
    private EntrepriseService entrepriseService;

    //INITIALISATION SERVICE
    @Autowired
    public EntrepriseController(final EntrepriseService entrepriseService){
        this.entrepriseService = entrepriseService;
    }

    //CREATE
    @RequestMapping(value = "/entreprises", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Add an enterprise")
    void addEntreprise(@RequestBody Entreprise entreprise){
        this.entrepriseService.create(entreprise);
    }

    //READ
    @RequestMapping(value = "/entreprises/{idEntreprise}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get an enterprise by id")
    Entreprise getEntrepriseById(@PathVariable final Integer idEntreprise) {
        return this.entrepriseService.read(idEntreprise);
    }

    //UPDATE
    @RequestMapping(value = "/entreprises/{idEntreprise}/", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update an entreprise")
    void updateEntreprise(@PathVariable final Integer idEntreprise, @PathVariable final Entreprise entreprise){
        Entreprise currentEntreprise = entrepriseService.read(idEntreprise);
        if(entreprise.getNomEntreprise() != null){
            currentEntreprise.setNomEntreprise(entreprise.getNomEntreprise());
        }
        if(entreprise.getAdresseEntreprise() != null){
            currentEntreprise.setAdresseEntreprise(entreprise.getAdresseEntreprise());
        }
        if(entreprise.getVilleEntreprise() != null){
            currentEntreprise.setVilleEntreprise(entreprise.getVilleEntreprise());
        }
        if(entreprise.getCodePostalEntreprise() != null){
            currentEntreprise.setCodePostalEntreprise(entreprise.getCodePostalEntreprise());
        }
        if(entreprise.getDateCreationEntreprise() != null){
            currentEntreprise.setDateCreationEntreprise(entreprise.getDateCreationEntreprise());
        }
        if(entreprise.getEmailEntreprise() != null){
            currentEntreprise.setEmailEntreprise(entreprise.getEmailEntreprise());
        }
        if(entreprise.getTelephoneEntreprise() != null){
            currentEntreprise.setTelephoneEntreprise(entreprise.getTelephoneEntreprise());
        }
       entrepriseService.update(currentEntreprise);
    }

    //DELETE
    @RequestMapping(value = "/entreprises/{idEntreprise}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete an enterprise")
    void deleteEntreprise(@PathVariable final Integer idEntreprise) {
        entrepriseService.delete(idEntreprise);
    }

    //READ ALL
    @RequestMapping(value = "/entreprises", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get all enterprises")
    List<Entreprise> getAllEntreprises() {
        return this.entrepriseService.readAll();
    }
}
