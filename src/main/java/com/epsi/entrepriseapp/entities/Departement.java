package com.epsi.entrepriseapp.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name="departement")
public class Departement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_departement")
    private int id;
    @Column(name="nom_departement")
    private String nomDepartement;
    @Column(name="budget_departement")
    private BigInteger budgetDepartement;
    @OneToMany(mappedBy = "departement")
    @JsonIgnore
    private List<Employe> employe;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomDepartement() {
        return nomDepartement;
    }

    public void setNomDepartement(String nomDepartement) {
        this.nomDepartement = nomDepartement;
    }

    public BigInteger getBudgetDepartement() {
        return budgetDepartement;
    }

    public void setBudgetDepartement(BigInteger budgetDepartement) {
        this.budgetDepartement = budgetDepartement;
    }
}
