package com.epsi.entrepriseapp.entities;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="employe")
public class Employe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_employe")
    private int id;
    @Column(name = "nom_employe")
    private String nomEmploye;
    @Column(name = "prenom_employe")
    private String prenomEmploye;
    @Column(name = "date_embauche_employe")
    private Date dateEmbaucheEmploye;
    @Column(name = "salaire_employe")
    private Float salaireEmploye;
    @Column(name = "poste_employe")
    private String posteEmploye;
    @Column(name = "email_employe")
    private String emailEmploye;
    @Column(name = "telephone_employe")
    private String telephoneEmploye;
    @Column(name = "mot_de_passe_employe")
    private String motDePasse;
    @ManyToOne
    private Departement departement;
    @ManyToOne
    private Entreprise entreprise;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomEmploye() {
        return nomEmploye;
    }

    public void setNomEmploye(String nomEmploye) {
        this.nomEmploye = nomEmploye;
    }

    public String getPrenomEmploye() {
        return prenomEmploye;
    }

    public void setPrenomEmploye(String prenomEmploye) {
        this.prenomEmploye = prenomEmploye;
    }

    public Date getDateEmbaucheEmploye() {
        return dateEmbaucheEmploye;
    }

    public void setDateEmbaucheEmploye(Date dateEmbaucheEmploye) {
        this.dateEmbaucheEmploye = dateEmbaucheEmploye;
    }

    public Float getSalaireEmploye() {
        return salaireEmploye;
    }

    public void setSalaireEmploye(Float salaireEmploye) {
        this.salaireEmploye = salaireEmploye;
    }

    public String getPosteEmploye() {
        return posteEmploye;
    }

    public void setPosteEmploye(String posteEmploye) {
        this.posteEmploye = posteEmploye;
    }

    public String getEmailEmploye() {
        return emailEmploye;
    }

    public void setEmailEmploye(String emailEmploye) {
        this.emailEmploye = emailEmploye;
    }

    public String getTelephoneEmploye() {
        return telephoneEmploye;
    }

    public void setTelephoneEmploye(String telephoneEmploye) {
        this.telephoneEmploye = telephoneEmploye;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }
}