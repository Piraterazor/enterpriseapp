package com.epsi.entrepriseapp.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jdk.jfr.Enabled;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name="entreprise")
public class Entreprise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_entreprise")
    private int id;
    @Column(name="nom_entreprise")
    private String nomEntreprise;
    @Column(name="adresse_entreprise")
    private String adresseEntreprise;
    @Column(name="ville_entreprise")
    private String villeEntreprise;
    @Column(name="code_postal_entreprise")
    private String codePostalEntreprise;
    @Column(name="date_creation_entreprise")
    private Date dateCreationEntreprise;
    @Column(name="email_entreprise")
    private String emailEntreprise;
    @Column(name="telephone_entreprise")
    private String telephoneEntreprise;
    @OneToMany(mappedBy = "entreprise")
    @JsonIgnore
    private List<Employe> employe;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomEntreprise() {
        return nomEntreprise;
    }

    public void setNomEntreprise(String nomEntreprise) {
        this.nomEntreprise = nomEntreprise;
    }

    public String getAdresseEntreprise() {
        return adresseEntreprise;
    }

    public void setAdresseEntreprise(String adresseEntreprise) {
        this.adresseEntreprise = adresseEntreprise;
    }

    public String getVilleEntreprise() {
        return villeEntreprise;
    }

    public void setVilleEntreprise(String villeEntreprise) {
        this.villeEntreprise = villeEntreprise;
    }

    public String getCodePostalEntreprise() {
        return codePostalEntreprise;
    }

    public void setCodePostalEntreprise(String codePostalEntreprise) {
        this.codePostalEntreprise = codePostalEntreprise;
    }

    public Date getDateCreationEntreprise() {
        return dateCreationEntreprise;
    }

    public void setDateCreationEntreprise(Date dateCreationEntreprise) {
        this.dateCreationEntreprise = dateCreationEntreprise;
    }

    public String getEmailEntreprise() {
        return emailEntreprise;
    }

    public void setEmailEntreprise(String emailEntreprise) {
        this.emailEntreprise = emailEntreprise;
    }

    public String getTelephoneEntreprise() {
        return telephoneEntreprise;
    }

    public void setTelephoneEntreprise(String telephoneEntreprise) {
        this.telephoneEntreprise = telephoneEntreprise;
    }
}
