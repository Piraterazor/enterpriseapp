package com.epsi.entrepriseapp.repositories;

import com.epsi.entrepriseapp.entities.Departement;
import com.epsi.entrepriseapp.entities.Entreprise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DepartementRepository extends JpaRepository<Departement, Integer> {
}

