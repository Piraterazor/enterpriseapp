package com.epsi.entrepriseapp.repositories;

import com.epsi.entrepriseapp.entities.Employe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeRepository extends JpaRepository<Employe, Integer> {

    List<Employe> findByNomEmploye(String nomEmploye);

    List<Employe> findByDepartementId(int idDepartement);

    Employe findByEmailEmploye(String email);

}
