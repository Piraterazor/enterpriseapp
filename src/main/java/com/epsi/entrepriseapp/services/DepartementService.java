package com.epsi.entrepriseapp.services;

import com.epsi.entrepriseapp.entities.Departement;

import java.util.List;

public interface DepartementService {
    public Departement create(Departement departement);     //C
    public Departement read(int id);			    //R
    public void update(Departement departement);	    //U
    public void delete(int id);	                //D
    public List<Departement> readAll();             //READ ALL
}
