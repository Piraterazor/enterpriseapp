package com.epsi.entrepriseapp.services;

import com.epsi.entrepriseapp.entities.Departement;
import com.epsi.entrepriseapp.entities.Employe;
import com.epsi.entrepriseapp.repositories.DepartementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartementServiceImpl implements DepartementService{

    @Autowired
    private DepartementRepository departementRepository;

    @Override
    public Departement create(Departement departement) {
        return departementRepository.save(departement);
    }

    @Override
    public Departement read(int id) {
        return departementRepository.findById(id).get();
    }

    @Override
    public void update(Departement departement) {
        departementRepository.save(departement);
    }

    @Override
    public void delete(int id) {
        departementRepository.deleteById(id);
    }

    @Override
    public List<Departement> readAll() {
        return (List<Departement>) departementRepository.findAll();
    }
}
