package com.epsi.entrepriseapp.services;

import com.epsi.entrepriseapp.entities.Employe;

import java.util.List;

public interface EmployeService {
    public Employe create(Employe employe);     //C
    public Employe read(int idEmploye);			    //R
    public void update(Employe employe);	    //U
    public void delete(int idEmploye);	                //D
    public List<Employe> readAll();             //READ ALL
    public List<Employe> getEmployesByName(String nomEmploye);
    public List<Employe> getEmployesByDepartementId(int idDepartement);
    public Employe getEmployeByEmail(String email);
}
