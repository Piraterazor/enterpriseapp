package com.epsi.entrepriseapp.services;

import com.epsi.entrepriseapp.entities.Employe;
import com.epsi.entrepriseapp.repositories.EmployeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeServiceImpl implements EmployeService{

    @Autowired
    private EmployeRepository employeRepository;

    @Override
    public Employe create(Employe employe) {
        return employeRepository.save(employe);
    }

    @Override
    public Employe read(int idEmploye) {
        return employeRepository.findById(idEmploye).get();
    }

    @Override
    public void update(Employe employe) {
        employeRepository.save(employe);
    }

    @Override
    public void delete(int idEmploye) {
        employeRepository.deleteById(idEmploye);
    }

    @Override
    public List<Employe> readAll() {
        return (List<Employe>) employeRepository.findAll();
    }

    @Override
    public List<Employe> getEmployesByName(String nomEmploye){
        return (List<Employe>)  employeRepository.findByNomEmploye(nomEmploye);
    }

    @Override
    public List<Employe> getEmployesByDepartementId(int idDepartement){
        return (List<Employe>)  employeRepository.findByDepartementId(idDepartement);
    }

    @Override
    public Employe getEmployeByEmail(String email){
        return employeRepository.findByEmailEmploye(email);
    }

}
