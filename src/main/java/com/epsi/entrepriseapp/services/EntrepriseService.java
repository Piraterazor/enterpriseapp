package com.epsi.entrepriseapp.services;

import com.epsi.entrepriseapp.entities.Employe;
import com.epsi.entrepriseapp.entities.Entreprise;

import java.util.List;

public interface EntrepriseService {
    public Entreprise create(Entreprise entreprise);     //C
    public Entreprise read(int id);			    //R
    public void update(Entreprise entreprise);	    //U
    public void delete(int id);	                //D
    public List<Entreprise> readAll();             //READ ALL
}
