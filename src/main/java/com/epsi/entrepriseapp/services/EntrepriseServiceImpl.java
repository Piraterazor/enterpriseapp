package com.epsi.entrepriseapp.services;

import com.epsi.entrepriseapp.entities.Entreprise;
import com.epsi.entrepriseapp.repositories.EntrepriseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntrepriseServiceImpl implements EntrepriseService{

    @Autowired
    private EntrepriseRepository entrepriseRepository;

    @Override
    public Entreprise create(Entreprise entreprise) {
        return entrepriseRepository.save(entreprise);
    }

    @Override
    public Entreprise read(int id) {
        return entrepriseRepository.findById(id).get();
    }

    @Override
    public void update(Entreprise entreprise) {
        entrepriseRepository.save(entreprise);
    }

    @Override
    public void delete(int id) {
        entrepriseRepository.deleteById(id);
    }

    @Override
    public List<Entreprise> readAll() {
        return (List<Entreprise>) entrepriseRepository.findAll();
    }

}
