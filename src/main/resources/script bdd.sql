--Script création BDD

CREATE TABLE entreprise(
   id_entreprise INT,
   nom_entreprise VARCHAR(50) NOT NULL,
   adresse_entreprise VARCHAR(50) NOT NULL,
   ville_entreprise VARCHAR(50) NOT NULL,
   code_postal_entreprise VARCHAR(50) NOT NULL,
   date_creation_entreprise DATE NOT NULL,
   email_entreprise VARCHAR(100) NOT NULL,
   telephone_entreprise VARCHAR(50) NOT NULL,
   PRIMARY KEY(id_entreprise)
);

CREATE TABLE departement(
   id_departement INT,
   nom_departement VARCHAR(50) NOT NULL,
   budget_departement BIGINT NOT NULL,
   PRIMARY KEY(id_departement)
);

CREATE TABLE employe(
   id_employe INT,
   nom_employe VARCHAR(50) NOT NULL,
   prenom_employe VARCHAR(50) NOT NULL,
   date_embauche_employe DATE NOT NULL,
   salaire_employe DECIMAL(15,2) NOT NULL,
   poste_employe VARCHAR(100) NOT NULL,
   email_employe VARCHAR(50) NOT NULL,
   telephone_employe VARCHAR(50) NOT NULL,
   mot_de_passe_employe VARCHAR(50) NOT NULL,
   departement_id_departement INT NOT NULL,
   entreprise_id_entreprise INT NOT NULL,
   PRIMARY KEY(id_employe),
   FOREIGN KEY(departement_id_departement) REFERENCES departement(id_departement),
   FOREIGN KEY(entreprise_id_entreprise) REFERENCES entreprise(id_entreprise)
);
