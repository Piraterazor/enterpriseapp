--Insertion Table Entreprise
INSERT INTO `Entreprise` (`id_entreprise`, `nom_entreprise`, `adresse_entreprise`, `ville_entreprise`, `code_postal_entreprise`, `date_creation_entreprise`, `email_entreprise`, `telephone_entreprise`) 
VALUES ('1', 'Pouchain', '04 allée des coquelicots', 'Lectrem', '58420', '2019-06-12', 'p.pouchain@hotmail.fr', '0344587459'),
('2', 'Gréco', '18 rue nobert', 'Paris', '70850', '2010-01-14', 'g.gréco@greg.fr', '0344487426'),
('3', 'Iléna', '02 boulevard de la liberté', 'Lille', '59800', '2020-08-25', 'c.ilena@gmail.com', '0344523694');

--Insertion Table Service
INSERT INTO `departement` (`id_departement`, `nom_departement`, `budget_departement`)
VALUES ('1', 'Achat', '500000'), 
('2', 'Ressource Humaine', '10000'), 
('3', 'Informatique', '100000'), 
('4', 'Comptabilité', '30000');

--Insertion Table Employe
INSERT INTO `employe` (`id_employe`, `nom_employe`, `prenom_employe`, `date_embauche_employe`, `salaire_employe`, `poste_employe`, `email_employe`, `telephone_employe`, `mot_de_passe_employe`, `departement_id_departement`, `entreprise_id_entreprise`) VALUES
(1, 'Famechon', 'Corentin', '2021-06-01', '1250.00', 'Chef de projet junior', 'c.famechon@staub.fr', '0612157479', 'cocodu6bulle', 3, 3),
(2, 'Lovriche', 'Loïc', '2020-01-01', '1420.00', 'Comptable', 'l.lovriche@ilena.fr', '0715698357', '1402', 4, 1),
(3, 'Voisin', 'Anaïs', '2019-09-11', '15235.00', 'Gestionnaire RH', 'a.voisin@gmail.com', '0741259479', 'test12', 2, 2),
(4, 'Mertrude', 'Martine', '2008-06-21', '1855.00', 'Directrice RH', 'm.m@ilena.fr', '0612154786', 'welcome', 2, 1),
(5, 'Caline', 'Gilette', '2015-12-05', '1287.00', 'Chef comptable', 'c.gilette@hotmail.fr', '0611458979', 'tolight', 4, 1),
(6, 'Facon', 'Sandrine', '2019-10-08', '4852.00', 'Approvisionneur', 's.facon@gmail.com', '0612126979', 'jungle', 1, 2),
(7, 'Ferchaud', 'Henri', '2018-07-06', '1456.00', 'Acheteur', 'h.ferchaud@cegetel.net', '0614857479', 'rlplayer', 1, 2),
(8, 'Vilbrequin', 'Anthony', '2014-06-15', '2593.00', 'manager réseau', 'a.vilbre@carrefour.fr', '0612157479', 'jaimelan', 3, 1),
(9, 'Sriène', 'Déréne', '2009-07-16', '3254.00', 'Comptable', 'd.sriene@orange.com', '0612157145', 'codelyoko', 4, 3),
(10, 'Dolche', 'Quich', '2020-02-12', '1254.00', 'Deal swift manager', 'd.quich@staub.fr', '0612597479', 'ecouteur', 3, 3);

